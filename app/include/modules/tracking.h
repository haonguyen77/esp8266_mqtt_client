/*
 * tracking.h
 *
 *  Created on: Sep 16, 2017
 *      Author: locso
 */

#ifndef MODULES_INCLUDE_TRACKING_H_
#define MODULES_INCLUDE_TRACKING_H_

#include "ets_sys.h"
#include "osapi.h"

#define CACHE_SIZE		10
#define MESSAGE_SIZE	600

typedef struct{
	char* data;
	int len;
	void* next;
	int sectionID;
}NODE;

typedef struct {
	NODE * front;
	NODE * rear;
	int count;
	int countID;
}QUEUE_CACHE;

typedef struct __attribute__((packed)){
	uint8_t enale;
	int start;
	int end;
}DEVICE_TIMER;

typedef struct __attribute__((packed, aligned(4)))
{
	uint8_t field1;
	uint8_t field2;
	uint8_t field3;
	uint8_t field4;
	float field5;
	float field6;
    DEVICE_TIMER timer1;
    DEVICE_TIMER timer2;
    DEVICE_TIMER timer3;
    char connected;
    uint64_t version;
} DEVICE_STATUS;

typedef struct __attribute__((packed)){
	int16_t year;			// year with all 4-digit (2011)
	int8_t month;			// month 1 - 12 (1 = Jan)
	int8_t mday;			// day of month 1 - 31
	int8_t wday;			// day of week 1 - 7 (1 = Sunday)
	int8_t hour;			// hour 0 - 23
	int8_t min;				// min 0 - 59
	int8_t sec;				// sec 0 - 59
} DATE_TIME;

extern DEVICE_STATUS state;

void ICACHE_FLASH_ATTR mqtt_service_init(void);

void ICACHE_FLASH_ATTR createReportedPackage(char *data, int *len, int msgID);
void ICACHE_FLASH_ATTR createDeiredPackage(char *data, int *len, int msgID);

char ICACHE_FLASH_ATTR updateDesired(void);
char ICACHE_FLASH_ATTR updateReported(void);
void ICACHE_FLASH_ATTR deltaHandler(char * buf);
void ICACHE_FLASH_ATTR getHandler(char *buf);
void ICACHE_FLASH_ATTR updateAcceptedHandler(char *buf);
void ICACHE_FLASH_ATTR configHandler(char * buf);

void ICACHE_FLASH_ATTR loop_cb(void);
void ICACHE_FLASH_ATTR main_loop_init(void);

void ICACHE_FLASH_ATTR sensor_init(void);

#endif /* MODULES_INCLUDE_TRACKING_H_ */
