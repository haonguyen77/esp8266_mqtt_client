/*
 * tracking.c
 *
 *  Created on: Sep 16, 2017
 *      Author: locso
 */
#include "tracking.h"

#include "ets_sys.h"
#include "driver/uart.h"
#include "osapi.h"
#include "mqtt.h"
#include "wifi.h"
#include "config.h"
#include "debug.h"
#include "gpio.h"
#include "user_interface.h"
#include "mem.h"
#include <stdlib.h>
#include <string.h>

#include "cJSON.h"

#include <inttypes.h>	// required for print uint64_t :))

/* Note
 * state.connected is controlled in:
 * 	wifi.c - wifi_check_ip 		Clear to 0 when not connected to AP
 * 	mqttConnectedCb 			Set to 1 when connected mqtt broker
 * 	mqttDisconnectedCb 			Clear to 0 when disconnect mqtt broker
 */

DEVICE_STATUS state;
DEVICE_STATUS state_old;
DATE_TIME sysTime;
QUEUE_CACHE publishCache;

float temperature = 0;
float humidity = 0;

uint8_t sw1_set = 0;
uint8_t sw2_set = 0;
uint8_t sw3_set = 0;
uint8_t sw4_set = 0;

DEVICE_TIMER timer1;
DEVICE_TIMER timer2;
DEVICE_TIMER timer3;

bool outputChanged = false;
bool publishedState = true;

char manual_trigger = 0;

static os_timer_t loop_timer;

char ICACHE_FLASH_ATTR pushCache(QUEUE_CACHE *Q, char* msg, int len);
char ICACHE_FLASH_ATTR popCache(QUEUE_CACHE *Q);
char ICACHE_FLASH_ATTR isFullCache(QUEUE_CACHE *Q);
char ICACHE_FLASH_ATTR isEmptyCache(QUEUE_CACHE *Q);

NODE* ICACHE_FLASH_ATTR createCache(char* msg, int len, int sID);
void ICACHE_FLASH_ATTR msgPublishedCb(QUEUE_CACHE *Q);
void ICACHE_FLASH_ATTR initCache(QUEUE_CACHE *Q);
void ICACHE_FLASH_ATTR sensor_scan();

char ICACHE_FLASH_ATTR * float2string(float val) {
	int whole, decimal;
	char *str;
	str=(char*)os_malloc(10);

	os_memset(str, 0x00, sizeof(str));

	whole = (int)val;
	decimal = (int)(val * 10 - whole * 10);
	os_sprintf(str, "%d.%d", whole, decimal);

	os_free(str);

	return str;
}

MQTT_Client mqttClient;

/*
 * MQTT Topic
 *
 *  Subcribe Topic:
 *  	device/thing/get/accepted	// get init version
 *  	device/thing/delta
 *  	device/thing/config
 *  Publish Topic
 *  	device/thing/update
 */
char topic_get[MESSAGE_SIZE];
char topic_update[MESSAGE_SIZE];
char topic_delta[MESSAGE_SIZE];
char topic_update_accepted[MESSAGE_SIZE];
char topic_update_rejected[MESSAGE_SIZE];

void ICACHE_FLASH_ATTR initCache(QUEUE_CACHE *Q){
	Q->front = NULL;
	Q->rear = NULL;
	Q->count = 0;
}

char ICACHE_FLASH_ATTR isEmptyCache(QUEUE_CACHE *Q)
{
	if(Q->count == 0) {
		return 1;
	}
	return 0;
}

char ICACHE_FLASH_ATTR isFullCache(QUEUE_CACHE *Q)
{
	if(Q->count == CACHE_SIZE) return 1;
	return 0;
}

NODE* ICACHE_FLASH_ATTR createCache(char* msg, int len, int sID)
{
	NODE *cache = (NODE*)os_zalloc(sizeof(NODE));
	cache->data = (char*)os_malloc(len);
	cache->next = NULL;
	os_sprintf(cache->data, msg);
	cache->len = len;
	cache->sectionID = sID;
	return cache;
}

char ICACHE_FLASH_ATTR pushCache(QUEUE_CACHE *Q, char* msg, int len)
{
	NODE* newCache;

	if(isFullCache(Q)){
		return 0;
	}else {
		newCache = createCache(msg, len, Q->countID);

		if(isEmptyCache(Q)){
			Q->front = newCache;
			Q->rear = newCache;
		}
		else{
			Q->rear->next = newCache;
			Q->rear = newCache;
		}

		Q->count++;
	}
	return 1;
}

char ICACHE_FLASH_ATTR popCache(QUEUE_CACHE *Q)
{
	if(isEmptyCache(Q)){
		return 0;
	}else {

		os_free(Q->front->data);
		os_free(Q->front);


		if(Q->count > 1){
			Q->front = (NODE*)Q->front->next;
			Q->count--;
		}
		else if(Q->count == 1){
			initCache(Q);
		}
		else{
			return 0;
		}
		return 1;
	}
}

void ICACHE_FLASH_ATTR msgPublishedCb(QUEUE_CACHE *Q)
{
	if(publishedState){
		if(isEmptyCache(Q) == 0){

			INFO("cache str: %s\n", Q->front->data);
			INFO("cache str len: %d\n", Q->front->len);
			MQTT_Publish(&mqttClient, topic_update, Q->front->data, Q->front->len, 1, 0);

			publishedState = false;
		}
	}
}

void ICACHE_FLASH_ATTR wifiConnectCb(uint8_t status)
{
	if(status == STATION_GOT_IP){
		MQTT_Connect(&mqttClient);
	} else {
		MQTT_Disconnect(&mqttClient);
	}
}

void ICACHE_FLASH_ATTR mqttConnectedCb(uint32_t *args)
{
	char *tmpBuf = (char *)os_zalloc(32);

	os_memset(topic_update, 0x00, sizeof topic_update);
	os_memset(topic_delta, 0x00, sizeof topic_delta);
	os_memset(topic_update_accepted, 0x00, sizeof topic_update_accepted);
	os_memset(topic_update_rejected, 0x00, sizeof topic_update_rejected);

	// Topic config
	os_sprintf(topic_get, "device/%s/get", sysCfg.device_id);
	os_sprintf(topic_update, "device/%s/update", sysCfg.device_id);
	os_sprintf(topic_delta, "device/%s/delta", sysCfg.device_id);
	os_sprintf(topic_update_accepted, "device/%s/update/accepted", sysCfg.device_id);
	os_sprintf(topic_update_rejected, "device/%s/update/rejected", sysCfg.device_id);

	MQTT_Client* client = (MQTT_Client*)args;

	INFO("MQTT: Connected\r\n");

	state.connected = 1;

	// Subcribe by deviceId
	MQTT_Subscribe(client, topic_get, 0);
	MQTT_Subscribe(client, topic_delta, 0);
	MQTT_Subscribe(client, topic_update_accepted, 0);
	MQTT_Subscribe(client, topic_update_rejected, 0);

	// Get device info
	MQTT_Publish(client, topic_get, "{}", 2, 1, 0);

	os_free(tmpBuf);
}

void ICACHE_FLASH_ATTR mqttDisconnectedCb(uint32_t *args)
{
	MQTT_Client* client = (MQTT_Client*)args;

	state.connected = 0;
	INFO("MQTT: Disconnected\r\n");
}

void ICACHE_FLASH_ATTR mqttPublishedCb(uint32_t *args)
{
	MQTT_Client* client = (MQTT_Client*)args;
	INFO("MQTT: Published\r\n");

	manual_trigger = 0;
	outputChanged = false;
}

void ICACHE_FLASH_ATTR mqttDataCb(uint32_t *args, const char* topic, uint32_t topic_len, const char *data, uint32_t data_len)
{
	char *topicBuf = (char*)os_zalloc(topic_len+1),
			*dataBuf = (char*)os_zalloc(data_len+1);

	INFO("num byte data %d\n", data_len);

	MQTT_Client* client = (MQTT_Client*)args;

	os_memcpy(topicBuf, topic, topic_len);
	topicBuf[topic_len] = 0;

	os_memcpy(dataBuf, data, data_len);
	dataBuf[data_len] = 0;

	INFO("Receive topic: %s, data: %s \r\n", topicBuf, dataBuf);

	// Topic get
	if (os_strcmp(topicBuf, topic_get) == 0) {
		getHandler(dataBuf);
	}

	// Topic delta
	if (os_strcmp(topicBuf, topic_delta) == 0) {
		deltaHandler(dataBuf);
	}

	if(os_strcmp(topicBuf, topic_update_accepted) == 0){
		updateAcceptedHandler(dataBuf);
	}
	os_free(topicBuf);
	os_free(dataBuf);
}

void ICACHE_FLASH_ATTR mqtt_service_init(void) {
	struct station_config wifi_config;

	INFO(" get configuration\r\n");
	os_memset(&sysCfg, 0x00, sizeof sysCfg);
	sysCfg.cfg_holder = CFG_HOLDER;

	// deviceId
	os_sprintf(sysCfg.device_id, MQTT_CLIENT_ID, system_get_chip_id());
	sysCfg.device_id[sizeof(sysCfg.device_id) - 1] = '\0';
	INFO("Device ID: %s\n",sysCfg.device_id);

	// Wifi settings
	//wifi_station_get_config(&wifi_config);
	INFO("SSID: %s\r\n", STA_SSID);
	INFO("PWD: %s\r\n", STA_PASS);
	os_strncpy(sysCfg.sta_ssid, STA_SSID, sizeof(sysCfg.sta_ssid) - 1);
	os_strncpy(sysCfg.sta_pwd, STA_PASS, sizeof(sysCfg.sta_pwd) - 1);
	sysCfg.sta_type = STA_TYPE;
	os_strncpy(sysCfg.mqtt_host, MQTT_HOST, sizeof(sysCfg.mqtt_host) - 1);
	sysCfg.mqtt_port = MQTT_PORT;
	os_strncpy(sysCfg.mqtt_user, MQTT_USER, sizeof(sysCfg.mqtt_user) - 1);
	os_strncpy(sysCfg.mqtt_pass, MQTT_PASS, sizeof(sysCfg.mqtt_pass) - 1);
	sysCfg.security = DEFAULT_SECURITY; /* default non ssl */
	sysCfg.mqtt_keepalive = MQTT_KEEPALIVE;

	MQTT_InitConnection(&mqttClient, sysCfg.mqtt_host, sysCfg.mqtt_port, sysCfg.security);
	//MQTT_InitConnection(&mqttClient, "192.168.11.122", 1880, 0);

	MQTT_InitClient(&mqttClient, sysCfg.device_id, sysCfg.mqtt_user, sysCfg.mqtt_pass, sysCfg.mqtt_keepalive, 1);
	//MQTT_InitClient(&mqttClient, "client_id", "user", "pass", 120, 1);

	MQTT_InitLWT(&mqttClient, "/lwt", "offline", 0, 0);
	MQTT_OnConnected(&mqttClient, mqttConnectedCb);
	MQTT_OnDisconnected(&mqttClient, mqttDisconnectedCb);
	MQTT_OnPublished(&mqttClient, mqttPublishedCb);
	MQTT_OnData(&mqttClient, mqttDataCb);

	WIFI_Connect(sysCfg.sta_ssid, sysCfg.sta_pwd, wifiConnectCb);
}


void ICACHE_FLASH_ATTR updateDeviceStatus(void) {

	// get SW
	state.field1 = sw1_set;
	state.field2 = sw2_set;
	state.field3 = sw3_set;
	state.field4 = sw4_set;

	// get Temperature
	state.field5 = temperature;

	// get Humidity
	state.field6 = humidity;

	// get Timer
	state.timer1.enale = timer1.enale;
	state.timer1.start = timer1.start;
	state.timer1.end   = timer1.end;

	state.timer2.enale = timer2.enale;
	state.timer2.start = timer2.start;
	state.timer2.end   = timer2.end;

	state.timer3.enale = timer3.enale;
	state.timer3.start = timer3.start;
	state.timer3.end   = timer3.end;
}

void ICACHE_FLASH_ATTR createReportedPackage(char *data, int *len, int msgID) {

	char *buff = data;
	char *tmpBuf = (char*)os_zalloc(600);

	os_memset(buff, 0x00, sizeof buff);

	os_sprintf(tmpBuf, "{");
	os_strcat(buff, tmpBuf);

	// reported
	os_sprintf(tmpBuf, "\"state\":{\"reported\": {\"field1\":{\"value\":\"%d\"},", state.field1);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"field2\": {\"value\":\"%d\"},", state.field2);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"field3\": {\"value\":\"%d\"},", state.field3) ;
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"field4\": {\"value\":\"%d\"},", state.field4) ;
	os_strcat(buff, tmpBuf);


	os_sprintf(tmpBuf, "\"field5\": {\"value\":\"%s\"},", float2string(temperature));
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"field6\": {\"value\":\"%s\"},", float2string(humidity));
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"timer\": {\"t1\": {\"enable\": \"%d\",\"start\": \"%d\", \"stop\": \"%d\"},", state.timer1.enale,state.timer1.start,state.timer1.end);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"t2\": {\"enable\": \"%d\",\"start\": \"%d\", \"stop\": \"%d\"},", state.timer2.enale,state.timer2.start,state.timer2.end);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"t3\": {\"enable\": \"%d\",\"start\": \"%d\", \"stop\": \"%d\"}},", state.timer3.enale,state.timer3.start,state.timer3.end);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"countID\": \"%d\"}}", msgID);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "}");
	os_strcat(buff, tmpBuf);

	os_free(tmpBuf);

	*len = strlen(buff);
}

void ICACHE_FLASH_ATTR createDeiredPackage(char *data, int *len, int msgID) {

	char *buff = data;
	char *tmpBuf = (char*)os_zalloc(64);

	os_memset(buff, 0x00, sizeof buff);

	os_sprintf(tmpBuf, "{");
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"state\":{\"desired\": {\"field1\":{\"value\":\"%d\"},", state.field1);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"field2\": {\"value\":\"%d\"},", state.field2);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"field3\": {\"value\":\"%d\"},", state.field3) ;
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"field4\": {\"value\":\"%d\"},", state.field4) ;
	os_strcat(buff, tmpBuf);


	os_sprintf(tmpBuf, "\"field5\": {\"value\":\"%s\"},", float2string(temperature));
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"field6\": {\"value\":\"%s\"},", float2string(humidity));
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"timer\": {\"t1\": {\"enable\": \"%d\",\"start\": \"%d\", \"stop\": \"%d\"},", state.timer1.enale,state.timer1.start,state.timer1.end);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"t2\": {\"enable\": \"%d\",\"start\": \"%d\", \"stop\": \"%d\"},", state.timer2.enale,state.timer2.start,state.timer2.end);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"t3\": {\"enable\": \"%d\",\"start\": \"%d\", \"stop\": \"%d\"}},", state.timer3.enale,state.timer3.start,state.timer3.end);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "\"countID\": \"%d\"}}", msgID);
	os_strcat(buff, tmpBuf);

	os_sprintf(tmpBuf, "}");
	os_strcat(buff, tmpBuf);

	os_free(tmpBuf);

	*len = strlen(buff);
}

char ICACHE_FLASH_ATTR updateReported(void)
{
	char msg[600];
	int len;

	// update param
	updateDeviceStatus();

	if (state.connected) {
		state.version++;

		if(isFullCache(&publishCache)){
			popCache(&publishCache);
		}

		publishCache.countID++;
		createReportedPackage(msg, &len, publishCache.countID);

		INFO("Publish REPORT\n");
		INFO("\nREPORTED: %s\n", msg);

		pushCache(&publishCache, msg, len);

		//MQTT_Publish(&mqttClient, topic_update, msg, len, 1, 0);

		// free memory


		return 0;
	} else {
		return 1;
	}
}

char ICACHE_FLASH_ATTR updateDesired(void)
{
	char msg[600];
	int len;

	// update param
	updateDeviceStatus();

	if (state.connected) {
		state.version++;

		if(isFullCache(&publishCache)){
			popCache(&publishCache);
		}

		publishCache.countID++;
		createDeiredPackage(msg, &len, publishCache.countID);

		INFO("Publish DESIRED\n");
		INFO("\nDESIRED: %s\n", msg);

		pushCache(&publishCache, msg, len);

		// free memory

		return 0;
	} else {
		return 1;
	}
}

void ICACHE_FLASH_ATTR getHandler(char *buf) {
	INFO("\n get Handler: %s\n",buf);
	deltaHandler(buf);
}

void ICACHE_FLASH_ATTR updateAcceptedHandler(char *buf){
	cJSON * root = cJSON_Parse(buf);
	int sectionID;
	INFO("\nUpdate Accepted Handler\n");

	if(!root){
		INFO("JSON error\n");
	}else{
		root = cJSON_GetObjectItem(root, "reply");

		if(!root){
			INFO("JSON syntax error");
		}
		else{
			if(os_strcmp(cJSON_GetObjectItem(root,"state")->valuestring,"OK") == 0){

				sectionID = cJSON_GetObjectItem(root,"sectionID")->valueint;
				INFO("Receive section ID: %d\n",sectionID);

				if(sectionID == publishCache.front->sectionID){
					publishedState = true;
					popCache(&publishCache);
				}
				else if(sectionID > publishCache.front->sectionID){

					while(sectionID >= publishCache.front->sectionID){
						popCache(&publishCache);
						if(isEmptyCache(&publishCache)) break;
					}
				}
				publishedState = true;
			}
		}
	}
}

void ICACHE_FLASH_ATTR getRejectedHandler(char *buf) {
	INFO("\n get rejected Handler\n");
	state.version = 1;
}

void ICACHE_FLASH_ATTR deltaHandler(char * buf) {
	int i, j;

	cJSON * root = cJSON_Parse(buf);
	cJSON * subitem;
	cJSON * timeritem;

	INFO("\nDelta Handler\n");

	if(!root){
		INFO("JSON error\n");
	}
	else{
		root = cJSON_GetObjectItem(root, "state");

		if(!root){
			INFO("JSON syntax error\n");
		}
		else{
			root =  cJSON_GetObjectItem(root, "delta");

			if(!root){
				INFO("JSON syntax error\n");
			}
			else{
				int num_item = cJSON_GetArraySize(root);

				for (i = 0 ; i < num_item ; i++){
					subitem = cJSON_GetArrayItem(root, i);

					os_printf("%s: %s\n", subitem->string, cJSON_Print(subitem));

					// Handle SubItem
					if(os_strcmp(subitem->string, "field1") == 0){
						sw1_set = (uint8_t)cJSON_GetObjectItem(subitem, "value")->valueint;
					}

					if(os_strcmp(subitem->string, "field2") == 0){
						sw2_set = (uint8_t)cJSON_GetObjectItem(subitem, "value")->valueint;
					}

					if(os_strcmp(subitem->string, "field3") == 0){
						sw3_set = (uint8_t)cJSON_GetObjectItem(subitem, "value")->valueint;
					}

					if(os_strcmp(subitem->string, "field4") == 0){
						sw4_set = (uint8_t)cJSON_GetObjectItem(subitem, "value")->valueint;
					}

					if(os_strcmp(subitem->string, "field5") == 0){
						temperature = (float)cJSON_GetObjectItem(subitem, "value")->valuedouble;
					}

					if(os_strcmp(subitem->string, "field6") == 0){
						humidity = (float)cJSON_GetObjectItem(subitem, "value")->valuedouble;
					}

					if(os_strcmp(subitem->string, "timer") == 0){
						int numtimer = cJSON_GetArraySize(subitem);
						os_printf("num timer item: %d\n",numtimer);

						for(j = 0; j < numtimer ; j++){
							timeritem = cJSON_GetArrayItem(subitem,j);

							if(os_strcmp(timeritem->string, "t1") == 0){
								state.timer1.enale = (uint8_t)cJSON_GetObjectItem(timeritem, "enable")->valueint;
								state.timer1.start = cJSON_GetObjectItem(timeritem, "start")->valueint;
								state.timer1.end   = cJSON_GetObjectItem(timeritem, "stop")->valueint;
							}

							if(os_strcmp(timeritem->string, "t2") == 0){
								state.timer2.enale = (uint8_t)cJSON_GetObjectItem(timeritem, "enable")->valueint;
								state.timer2.start = cJSON_GetObjectItem(timeritem, "start")->valueint;
								state.timer2.end   = cJSON_GetObjectItem(timeritem, "stop")->valueint;
							}

							if(os_strcmp(timeritem->string, "t3") == 0){
								state.timer3.enale = (uint8_t)cJSON_GetObjectItem(timeritem, "enable")->valueint;
								state.timer3.start = cJSON_GetObjectItem(timeritem, "start")->valueint;
								state.timer3.end   = cJSON_GetObjectItem(timeritem, "stop")->valueint;
							}
						}
					}

					if(os_strcmp(subitem->string, "countID") == 0)
					{
						os_printf("section ID: %d\n", cJSON_GetObjectItem(subitem,"value")->valueint);
					}
				}

				//report
				updateReported();

				cJSON_Delete(subitem);
				cJSON_Delete(timeritem);
			}
		}
	}
	cJSON_Delete(root);
}

void ICACHE_FLASH_ATTR timeDisplay(uint64_t time) {
	int day, hour, min, sec;

	day = time / (60*60*24);
	time = time % (60*60*24);
	hour = time / (60*60);
	time = time % (60*60);
	min = time / 60;
	sec = time % 60;

	INFO("\n\nsysTime: %d days %d : %d : %d\n\n", day, hour, min, sec);
}

void ICACHE_FLASH_ATTR loop_cb(void) {
	static uint64_t cnt = 0;
	static uint64_t runtime = 0;

	os_timer_disarm(&loop_timer);

	cnt++;

	// Scan Published Message
	msgPublishedCb(&publishCache);

	// Scan Input Change
	//input_scan();

	if (cnt%10 == 0) { // 1s
		runtime++;

		// Scan sensor
		sensor_scan();

		// Update sensor
		updateDeviceStatus();
	}

	if (outputChanged) {
		if (state.connected) {
			if (manual_trigger)  { 	// local control
				os_printf("DEBUG: MANUAL CONTROL => UPDATE DESIRED\n");
				updateDesired();
			} else {				// remote control
				os_printf("DEBUG: STATE CHANGED ===> UPDATE SHADOWS\n");
				updateReported();
			}
		}
	}

	if (cnt%100 == 0) {
		// Reported shadow
		updateReported();

		timeDisplay(runtime);
	}

	os_timer_setfn(&loop_timer, (os_timer_func_t *)loop_cb, NULL);
	os_timer_arm(&loop_timer, 100, 1);
}

//void ICACHE_FLASH_ATTR input_scan(void)	// 100 ms
//{
//	static int sc_cnt = 0;
//	static int sc_timeout = 0;

//	char state;



	// Control


//	// Update desired
//	if (outputChanged) {
//		outputChanged = false;
//		os_printf("DEBUG: STATE CHANGED ===> UPDATE SHADOWS\n");
//		updateDesired();
//	}

	// Scan SmartConfig
//}

void ICACHE_FLASH_ATTR main_loop_init(void) {

	//io_init();

	// Init Modbus Sensor
	//myMB_Init(9600);

	initCache(&publishCache);

	// Loop Callback
	os_timer_setfn(&loop_timer, (os_timer_func_t *)loop_cb, NULL);
	os_timer_arm(&loop_timer, 100, 1);
}


void ICACHE_FLASH_ATTR sensor_scan(void) {

}

void ICACHE_FLASH_ATTR sensor_init(void)
{
//	// Init
//	//Disarm timer
//	os_timer_disarm(&sensor_timer);
//	//Setup timer
//	os_timer_setfn(&sensor_timer, (os_timer_func_t *)sensor_timerfunc, NULL);
//	//Arm timer for every 10 sec.
//	os_timer_arm(&sensor_timer, 2000, 1);
//
//    //system_os_task(user_procTask, user_procTaskPrio,user_procTaskQueue, user_procTaskQueueLen);
}
